package com.zempty.springgateway.config;

/**
 * @author zempty
 * @ClassName GateWayConfig.java
 * @Description 通过配置 java 类代替配置文件进行路由
 * @createTime 2020年12月19日 15:57:00
 */

import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class GateWayConfig {


    @Bean
    public RouteLocator routeLocator1(RouteLocatorBuilder builder) {
        //google1 是 id ,紧接着是路由的路径，和跳转的路径
        return builder.routes()
                .route("javaconfig1", s -> s.path("/google")
                .uri("https://www.google.com/"))
                .build();
    }
}
