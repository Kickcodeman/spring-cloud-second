package com.zempty.cloudprovider2payment8002;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = "com.zempty")
@EnableDiscoveryClient
public class CloudProvider2Payment8002Application {

    public static void main(String[] args) {
        SpringApplication.run(CloudProvider2Payment8002Application.class, args);
    }

}
