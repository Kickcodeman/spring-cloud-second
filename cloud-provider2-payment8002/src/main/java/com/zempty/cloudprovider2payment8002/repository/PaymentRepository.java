package com.zempty.cloudprovider2payment8002.repository;

import com.zempty.cloudprovider2payment8002.entity.Payment;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author zempty
 * @ClassName PaymentRepository.java
 * @Description 用来数据库的查询
 * @createTime 2020年11月11日 23:13:00
 */
public interface PaymentRepository extends JpaRepository<Payment,Integer> {
}
