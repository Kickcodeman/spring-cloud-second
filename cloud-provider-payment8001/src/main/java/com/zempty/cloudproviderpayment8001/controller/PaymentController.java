package com.zempty.cloudproviderpayment8001.controller;

import com.zempty.cloudproviderpayment8001.entity.Payment;
import com.zempty.cloudproviderpayment8001.repository.PaymentRepository;
import com.zempty.response.web.WebResponse;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

/**
 * @author zempty
 * @ClassName PaymentController.java
 * @Description 统一处理 payment 控制器
 * @createTime 2020年11月30日 23:48:00
 */
@RestController
@AllArgsConstructor
@RequestMapping("/payment")
public class PaymentController {

    private final PaymentRepository paymentRepository;


    @PostMapping(value = "/save")
    public Payment savePayment(@RequestBody Payment payment) {
        return payment;
    }


    // 测试返回 String
    @GetMapping(value = "/test")
    public WebResponse test() {
        return WebResponse.success("provider1...................................");
    }



    // 测试空值
    @GetMapping("test2")
    public void test2() {
    }


    @GetMapping("test3")
    public String test3() {
        int i = 1 / 0;
        return "test";
    }

}
