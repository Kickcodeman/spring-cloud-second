package com.zempty.cloudproviderpayment8001;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = "com.zempty")
@EnableDiscoveryClient
public class CloudProviderPayment8001Application {

	public static void main(String[] args) {
		SpringApplication.run(CloudProviderPayment8001Application.class, args);
	}

}
