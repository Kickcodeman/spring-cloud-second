package com.zempty.cloudproviderpayment8001.entity;

/**
 * @author zempty
 * @ClassName Payment.java
 * @Description 描述 payment 的功能类
 * @createTime 2020年11月11日 23:02:00
 */

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;

@Setter
@Getter
@Accessors(chain = true)
@Entity
public class Payment implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String serial;

}
