package com.zempty.response.web;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @author zempty
 * @ClassName WebResponse.java
 * @Description 用来统一返回值信息
 * @createTime 2020年11月30日 23:11:00
 */
@Setter
@Getter
@Accessors(chain = true)
public class WebResponse implements Serializable {


    private String message;

    private Integer status;

    private Object data;

    public static  WebResponse success(Object data) {
        return new WebResponse().setStatus(1)
                .setData(data);
    }

    public static WebResponse success() {
        return new WebResponse().setStatus(1);
    }

    public static WebResponse failure(String message) {
        return new WebResponse().setStatus(-1)
                .setMessage(message);
    }
}
