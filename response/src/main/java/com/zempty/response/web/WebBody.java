package com.zempty.response.web;

import com.zempty.response.exception.ExceptionMessage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

import javax.servlet.http.HttpServletRequest;

/**
 * @author zempty
 * @ClassName ResponseBodyAdvice.java
 * @Description 统一返回 json 字符串
 * @createTime 2020年11月30日 23:32:00
 */
@RestControllerAdvice
@Slf4j
public class WebBody implements ResponseBodyAdvice {





    @ExceptionHandler(Exception.class)
    private WebResponse handleExcepiton(Exception e) {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        String url = request.getRequestURL().toString();
        System.out.println(url);
        System.out.println("======================="+url+" 接口出现错误 "+"====================================");
        e.printStackTrace();
        if(e instanceof ExceptionMessage)
            return WebResponse.failure(e.getMessage());
        else
            return WebResponse.failure(e.toString());

    }


    @Override
    public boolean supports(MethodParameter returnType, Class converterType) {
        return !returnType.getGenericParameterType().equals(WebResponse.class);
    }

    @Override
    public Object beforeBodyWrite(Object body, MethodParameter returnType, MediaType selectedContentType, Class selectedConverterType, ServerHttpRequest request, ServerHttpResponse response) {
        System.out.println("进来了。。。。。。。。。");
        return WebResponse.success(body);

    }
}
