package com.zempty.response.exception;

import lombok.Getter;
import lombok.Setter;

/**
 * @author zempty
 * @ClassName ExceptionMessage.java
 * @Description 用来统一返回错误的异常信息
 * @createTime 2020年11月30日 23:09:00
 */

@Setter
@Getter
public class ExceptionMessage extends Exception{



    private String message;

    public ExceptionMessage(String message) {
        this.message = message;
    }


}
